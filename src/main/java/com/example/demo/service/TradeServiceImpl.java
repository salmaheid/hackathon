package com.example.demo.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.example.demo.entities.Trade;
import com.example.demo.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {
    @Autowired
    private TradeRepository repo;

    @Override
    public Collection<Trade> getTrades() {
        return repo.findAll();
    }
    @Override
    public void addTrade(Trade trade) {
        repo.insert(trade);
    }
    @Override
    public void updateTrade(Trade s) {
        repo.deleteByStockTicker(s.getStockTicker());
        repo.save(s);
    }
    @Override
    public Collection<Trade> getByStockTicker(String name){
        return repo.getByStockTicker(name);
    }
    @Override
    public Collection<Trade> deleteByStockTicker(String name){
        return repo.deleteByStockTicker(name);
    }

    /*
    @Override
    public void removeStock(Stock s) {        
        repo.delete(s);
    }
    @Override
    public void delete(ObjectId id) {
        repo.deleteById(id);
    }
    */
}   