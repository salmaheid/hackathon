package com.example.demo.service;

import java.util.Collection;
import com.example.demo.entities.Trade;

import org.bson.types.ObjectId;

public interface TradeService {
    //Stock getStock(String id);
    Collection<Trade> getTrades() ;
    void addTrade(Trade s);
    void updateTrade(Trade s);
    Collection<Trade> deleteByStockTicker(String name);
    Collection<Trade> getByStockTicker(String name);


    /*
    public List<Stock> findAll() {
        // TODO Auto-generated method stub
        return null;
    }

    
    public List<Stock> findAll(Sort sort) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public <S extends Stock> List<S> findAll(Example<S> example) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public <S extends Stock> List<S> findAll(Example<S> example, Sort sort) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public <S extends Stock> S insert(S entity) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public <S extends Stock> List<S> insert(Iterable<S> entities) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public <S extends Stock> List<S> saveAll(Iterable<S> entities) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public Page<Stock> findAll(Pageable pageable) {
        // TODO Auto-generated method stub
        return null;
    }

    
    public long count() {
        // TODO Auto-generated method stub
        return 0;
    }

    
    public void delete(Stock entity) {
        // TODO Auto-generated method stub

    }

    
    public void deleteAll() {
        // TODO Auto-generated method stub

    }

    
    public void deleteAll(Iterable<? extends Stock> entities) {
        // TODO Auto-generated method stub

    }

    
    public void deleteById(ObjectId id) {
        // TODO Auto-generated method stub

    }

    
    public boolean existsById(ObjectId id) ;
    
    public Iterable<Stock> findAllById(Iterable<ObjectId> ids) ;
    
    public Optional<Stock> findById(ObjectId id) ;

    public <S extends Stock> S save(S entity) ;

    
    public <S extends Stock> long count(Example<S> example) ;

    
    public <S extends Stock> boolean exists(Example<S> example);
    
    public <S extends Stock> Page<S> findAll(Example<S> example, Pageable pageable);
    public <S extends Stock> Optional<S> findOne(Example<S> example);
    */
}