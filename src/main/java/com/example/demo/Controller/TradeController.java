package com.example.demo.Controller;

import com.example.demo.service.TradeService;
import com.example.demo.service.TradeServiceImpl;

import java.util.Collection;

import com.example.demo.entities.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/trades")
public class TradeController {
    @Autowired
    private TradeService service;
    @GetMapping("/")
    public Collection<Trade> getTrades(){
        return service.getTrades();
    }
    @PostMapping("/")     
    public void addTrade(@RequestBody Trade stock){
        service.addTrade(stock);
    }
    @PutMapping("/")
    public void updateTrade(@RequestBody Trade stock){
        service.updateTrade(stock);
    }
    @GetMapping("/name")
    public Collection<Trade> getTradeName(@RequestParam("name") String name){
        return service.getByStockTicker(name);
    }

    //Change name to DeleteMapping for added security
    @GetMapping("/del/name")
    public Collection<Trade> deleteTrade(@RequestParam("name") String name){
        return service.deleteByStockTicker(name);
    }
/*
    @DeleteMapping("/")
    public void delete(@RequestParam("id") String id){
        return service.delete(id);
    }
*/
}