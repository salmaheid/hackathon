package com.example.demo.entities;

import java.io.*;
import java.util.*;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Portfolio {
    @Id
    private ObjectId id;
    private ArrayList<Security> securities;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ArrayList<Security> getSecurities() {
        return securities;
    }

    public void setSecurities(ArrayList<Security> securities) {
        this.securities = securities;
    }

    /*
     TODO
     */
    public void getStockByName(){

    }
    public void removeSecurityFromPortfolio(){
        
    }
  
}