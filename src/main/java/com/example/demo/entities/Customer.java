package com.example.demo.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Customer {
    @Id
    private ObjectId id;

    private Portfolio portfolio;
    private String name;
    private String address;
    private double money;
    private String currencyUsed;

    public boolean sellStock(){
        return false;
    }

}