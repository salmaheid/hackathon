package com.example.demo.entities;

public abstract class Security {
    private String name;
    private String dateCreated;
    private String owner_id;
    private String state;
    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


}