package com.example.demo.repo;

import java.util.Collection;

import com.example.demo.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade,ObjectId> {  
    Collection<Trade> getByStockTicker(String name);
    Collection<Trade> deleteByStockTicker(String name);

}