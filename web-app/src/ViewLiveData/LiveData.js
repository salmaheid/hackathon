import React from 'react';
import LiveDataService from "../Services/LiveDataService.js";
import axios from 'axios';

class LiveData extends React.Component {
    constructor(props) {
        super(props);
        //Needs to store users and 
        this.state = {ticker: "", liveData: undefined};
//        this.displayFriendsOnSelect = this.displayFriendsOnSelect.bind(this);     
        this.updateTicker = this.updateTicker.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      getDataByTicker(ticker){
        //Gets StockPrice of Apple for current time
        axios.get("https://cors-anywhere.herokuapp.com/https://finnhub.io/api/v1/quote?symbol="+ ticker + "&token=btfpj4f48v6rl6gbolsg",
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            'x-requested-with': 'foo',
          'X-Finnhub-Secret': 'btfpj4f48v6rl6gbolt'
          }
        }
        ).then(response => {
            this.setState({liveData:response.data.c})
        });;
    }
      
      updateTicker= (event) => {
        var newTicker = event.target.value;
        this.setState({ticker: newTicker});
      }
      handleSubmit = (event) => {
          event.preventDefault();
          this.getDataByTicker(this.state.ticker);
          //this.setState({liveData: data})
      }
      render() {  
        return (

          <div>
            
            <button type="button">Buy</button>
            <button type="button">Sell</button>
            <form onSubmit={this.handleSubmit}>
                <label>
                Search Stock:
                <input type="text" value={this.state.ticker} onChange={this.updateTicker} />
                </label>
                <input type="submit" value="Submit" />
            </form>
            {
                this.state.liveData != undefined ?
                <p> Price of {this.state.ticker} is : ${this.state.liveData} </p>
                :
                <p> Enter a stock name to get it's price</p>
            }
          </div>   
          );
        }
        
      }
      
export default LiveData;