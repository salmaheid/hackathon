
import axios from "axios";
export default axios.create({
    baseURL: "http://localhost8080/trades",
    headers: {
        "Content-type": "application/json"
    }
});